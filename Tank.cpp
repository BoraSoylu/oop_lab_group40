#include"Tank.h"
#include<iostream>
#include<string>

using namespace std;

/**
 * This Function Constructs the Tank Class
 */
Tank::Tank() {
	setType("Tank");
}

/**
 * This Function Constructs the Tank Class
 *@param capacity is the capacity of tank
 */
Tank::Tank(double capacity) {
	this->capacity = capacity;
	fuel_quantity = 0;
	setId(count);
	count++;
	setType("Tank");
}

/**
 * This Function Deconstructs the Tank Class
 */
Tank::~Tank() {


}

/**
 * This Function Opens the Tanks Valve
 */
void Tank::open_valve() {
	if (isConnect == 1)
	{
		this->valve = 1;
	}
}

/**
 * This Function Closes the Tanks Valve
 */
void Tank::close_valve() {

	this->valve = 0;
}

/**
 * This Function Breaks the Fuel Tank
 */
void Tank::break_fuel_tank() {

	broken = 1;
}

/**
 * This Function Repairs the Fuel Tank
 */
void Tank::repair_fuel_tank() {

	broken = 0;
}

/**
 * This Function Gives the Current Fuel Quantity
 */
double Tank::getFuel_quantity() {
	return fuel_quantity;
}

/**
 * This Function Sets The Given Fuel Quantity
 *@param fuel_quantity Gives the Wanted Quantity
 */
void Tank::setFuel_quantity(double fuel_quantity) {
	this->fuel_quantity = fuel_quantity;
}

/**
 * This Function Gives the Current Tanks id
 */
//int Tank::getId() {
//	return id;           to do
//}

/**
 * This Function Fills the Tank
 *@param n is quantity of fuel
 */
void Tank::fill_tank(double n) {
	if (this->broken == 1)
	{
		return;
	}

	if (getFuel_quantity() + n > capacity) {
		cout << "Not enough capacity, some fuel was spilled."<<endl;
		setFuel_quantity(capacity);
	}
	else
	{
		setFuel_quantity(getFuel_quantity() + n);
	}
}

/**
 * This Function Connects the Tanks to the Engine
 */
void Tank::Connect() {
	if (broken == 1)
	{
		return;
	}

	isConnect = 1;
}

/**
 * This Function Disconnects the Tanks From Engine
 */
void Tank::Disconnect() {
	if (valve == 1) {
		cout << "Valve is open!! You cannot disconnect." << endl;
	}
	else
		isConnect = 0;
}

/**
 * This Function Gives Tanks Connection �nfo
 */
bool Tank :: getconnectstatus()
{
	return isConnect;
}

/**
 * This Function Gives Tanks Broken Status
 */
bool Tank::isbroken()
{
	return broken;
}

/**
 * This Function Gives the Wanted Tanks Capacity
 */
double Tank :: getcapacity()
{
	return this->capacity;
}

/**
 * This Function Gives Wanted Tanks Valve Status
 */
bool Tank::getvalvestatus()
{
	return this->valve;
}

/**
 * This Function Sets the Tanks Capacity
 */
void Tank :: setcapacity(double capacity)
{
	this->capacity = capacity;
}

/**
 * This Function prints that the simulation is stopped when the simulation is stopped
 */
string Tank::update() {
	string return_s = "Tank ";
	string id_s = to_string(getId());
	return_s += id_s;
	return_s.append(": Simulation stopped\n");

	return return_s;
}
/**
 * Copy Constructer
 */
Tank::Tank(const Tank& tank) {
	this->broken = tank.broken;
	this->capacity = tank.capacity;
	this->count = tank.count;
	this->fuel_quantity = tank.fuel_quantity;
	setId(tank.getId());
	this->valve = tank.valve;
}

