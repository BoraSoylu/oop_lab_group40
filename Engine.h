#pragma once
#include"Tank.h"
#include"Update.h"
#include <vector>
using namespace std;
class Engine:public Update
{
public:
	~Engine();
	void start_engine(void);
	void stop_engine(void);
	void give_back_fuel(double);
	void remove_fuel_tank(int);
	void add_fuel_tank(int capacity);
	void connect_fuel_tank_to_engine(int tank_id);
	void disconnect_fuel_tank_from_engine(int tank_id);
	double gettotal_consumed_fuel_quantity(void);
	void wait(int);
	void fuel_consumption(void);
	static Engine *getInstance(void) {
		if (!Instance) {
			Instance = new Engine;
		}
		return Instance;
	}
	vector<Tank> getTank();
	void setTank(vector<Tank>);
	bool getStatus();
	Tank getEngineTank();
	string update(); //new prints "Simulation stopped"


private:
	static Engine *Instance;
	Engine();
	double fuel_per_second;
	double total_consumed_fuel;
	bool status;
	Tank engineTank;
    vector <Tank> tank;
};

