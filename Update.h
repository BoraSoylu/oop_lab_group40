#pragma once
#include<iostream>
using namespace std;

class Update
{
private:
	string type=" ";
	int id;
public:
	void setType(string type) {
		this->type=type;
	}
	string getType() {
		return type;
	}
	int getId()const {
		return id;
	}
	void setId(int id) {
		this->id = id;
	}
	virtual string update() = 0;
};

