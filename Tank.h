#pragma once
#include"Update.h"
class Tank:public Update {

	double capacity;
	double fuel_quantity;
	bool broken = 0;
	static int count;
	bool isConnect=0;
	bool valve=0;
	
public:
	Tank(const Tank& tank);
	void setcapacity(double);
	bool getvalvestatus();
	double getcapacity();
	bool isbroken();
	bool getconnectstatus();
	void Connect();
	void Disconnect();
	//int getId(); to do
	Tank(double);
	Tank();
	~Tank();
	void open_valve();
	void close_valve();
	void break_fuel_tank();
	void repair_fuel_tank();
	double getFuel_quantity();
	void setFuel_quantity(double);
	void fill_tank(double);
	string update(); 
};