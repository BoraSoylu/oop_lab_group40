#include"Simulation.h"
#include<iostream>
#include<iomanip>

using namespace std;

/**
 * This Function Constructs the Simulation Class
 */
Simulation::Simulation(string in,string out)
{
	attach(engine);
	readfile(in,out);
}

/**
 * This Function Deconstructs the Simuation Class
 */
Simulation::~Simulation()
{
}
/**
 * This Function  Stops Simulation
 */
void Simulation::stop_simulation()
{
	operation_state = false;
	notify();
}

/**
 * This Function Reads all Commands
 */
void Simulation::readfile(string input,string output)
{
	
	in.open(input);
	if (in.fail()) {
		cout << "Input file couldn't open." << endl;
		system("pause");
		exit(0);
	}
	/*cout << "Enter an output file name : ";
	cin >> output;*/
	out.open(output);
	if (out.fail()) {
		cout << "Output file couldn't open." << endl;
		system("pause");
		exit(0);
	}
	string line;
	getline(in, line);
	line = clearCommmand(line);
	while (1) {
		if (!in) {
			cout << "Input file empty!\n";
			out << "Input file empty!\n";
			break;
		}
		if (line == "start_engine") {
			if (engine->getStatus()) {
				out << "Engine already running.\n";
			}
			else {
				engine->start_engine();
			}
		}
		else if (line == "stop_engine") {
			if (!engine->getStatus()) {
				out << "Engine already stopped.\n";
			}
			else {
				engine->stop_engine();
			}
		}
		else if (line == "list_fuel_tanks") {
			list_fuel_tanks();
		}
		else if (line == "print_fuel_tank_count") {
			print_fuel_tank_count();
		}
		else if (line == "list_connected_tanks") {
			list_connected_tanks();
		}
		else if (line == "print_total_fuel_quantity") {
			print_total_fuel_quantity();
		}
		else if (line == "print_total_consumed_fuel_quantity") {
			print_total_consumed_fuel_quantity();
		}
		else if (line == "stop_simulation") {
			stop_simulation();
			break;
		}
		else
		{
			string hold = line;
			int number = -1;
				number = findNumber(line);
				if (number != -1) {
					if (line == "give_back_fuel") {
						engine->give_back_fuel(number);
					}
					else if (line == "add_fuel_tank") {
						engine->add_fuel_tank(number);
						Tank* temp = new Tank;
						*temp=engine->getTank().back();
						attach(temp);
					}
					else if (line == "remove_fuel_tank") {
						if (vector_id_finder(number) != -1)
						{
							detach(&(engine->getTank().at(vector_id_finder(number))));
							engine->remove_fuel_tank(vector_id_finder(number));
						}
						else
							out << "ERROR !!" << endl;
					}
					else if (line == "connect_fuel_tank_to_engine") {
						if (vector_id_finder(number) != -1) {
							if (!engine->getTank().at(vector_id_finder(number)).getconnectstatus())
								engine->connect_fuel_tank_to_engine(number);
						}
					}
					else if (line == "disconnect_fuel_tank_from_engine") {
						if (vector_id_finder(number) != -1) {

							if (engine->getTank().at(vector_id_finder(number)).getconnectstatus())
								engine->disconnect_fuel_tank_from_engine(number);
						}
					}
					else if (line == "open_valve") {
						if (vector_id_finder(number) != -1)
						{
							temp = engine->getTank();
							temp.at(vector_id_finder(number)).open_valve();
							engine->setTank(temp);
						}
						else
							out << "ERROR !!" << endl;
					}
					else if (line == "close_valve") {
						if (vector_id_finder(number) != -1)
						{
							temp = engine->getTank();
							temp.at(vector_id_finder(number)).close_valve();
							engine->setTank(temp);

						}
						else
							out << "ERROR !!" << endl;
					}
					else if (line == "break_fuel_tank") {
						if (vector_id_finder(number) != -1)
						{
							temp = engine->getTank();
							temp.at(vector_id_finder(number)).break_fuel_tank();
							engine->setTank(temp);
						}
						else
							out << "ERROR !!" << endl;
					}
					else if (line == "repair_fuel_tank") {
						if (vector_id_finder(number) != -1)
						{
							temp = engine->getTank();
							temp.at(vector_id_finder(number)).repair_fuel_tank();
							engine->setTank(temp);
						}
						else
							out << "ERROR !!" << endl;
					}
					else if (line == "print_tank_info") {
						if (vector_id_finder(number) != -1)
						{
							print_tank_info(number);
						}
						else
							out << "ERROR !!" << endl;
					}
					else if (line == "wait") {
						engine->wait(number - 1);
					}
					else {
						line = hold;
						int number2;
						number = find2Number(line, number2);
						if (line == "fill_tank") {
							if (vector_id_finder(number) != -1) {
								temp = engine->getTank();
								temp.at(vector_id_finder(number)).fill_tank(number2);
								engine->setTank(temp);
							}
							else {
								out << "Couldn't find tank " << number << " to fill." << endl;
							}
						}
					}
				}
		}
		getline(in, line);
		line = clearCommmand(line);
		if (engine->getStatus()) {
			engine->fuel_consumption();
		}
	}
}

/**
 * This Function Clears Commands
 *@param a gets the Strings
 */
string Simulation::clearCommmand(string a) {
	string b;
	for (int i = 0; i < a.length(); i++) {
		if (a[i] == ';')
			break;
		b.push_back(a[i]);
	}
	return b;
}
/**
 * This Function Finds Commands
 *@param a gets the Strings
 */
int Simulation::findNumber(string &a) {
	bool flag = 1;
	int number;
	string b, num;
	stringstream ss;
	for (int i = 0; i < a.length(); i++) {
		if (flag == 1) {
			if (a[i] == ' ') {
				flag = 0;
				i++;
			}
			if (flag == 1) {
				b.push_back(a[i]);
			}
		}
		if (flag == 0) {
			num.push_back(a[i]);
		}
	}
	ss << num;
	ss >> number;
	a = b;
	return number;
}
/**
 * This Function Clears Commands
 *@param a gets the Strings
 *@number2 gets the number of command
 */
int Simulation::find2Number(string &a,int &number2) {
	bool flag = 1;
	int number;
	string b, num;
	stringstream ss;
	for (int i = 0; i < a.length(); i++) {
		if (flag == 1) {
			if (a[i] == ' ') {
				flag = 0;
				i++;
			}
			if (flag == 1) {
				b.push_back(a[i]);
			}
		}
		if (flag == 0) {
			num.push_back(a[i]);
		}
	}
	ss << num;
	ss >> number>>number2;
	a = b;
	return number;
}

/**
 * This Function Prints Total Consumed Fuel Quentity
 */
void Simulation::print_total_consumed_fuel_quantity() {
	out <<"Total consumed fuel quantity is " << engine->gettotal_consumed_fuel_quantity()<<endl;
}

/**
 * This Function Lists Fuel Tanks
 */
void Simulation ::list_fuel_tanks()
{
	/*if (engine->getTank().size() == 0)
	{
		out << "There are no fuel tanks to list.\n";
		return;
	}*/

	out << "Tank id     Capcaity     Current Quantity     IsBroken       Isconnect       Valve" << endl;
	out <<"Engine Tank   " << engine->getEngineTank().getcapacity() << setw(18) << engine->getEngineTank().getFuel_quantity()<<endl;
	for (int i = 0; i < engine->getTank().size(); i++)
	{
		out << engine->getTank().at(i).getId() << setw(16) << engine->getTank().at(i).getcapacity() << setw(16) << engine->getTank().at(i).getFuel_quantity();
		out << setw(16) << engine->getTank().at(i).isbroken() << setw(16) << engine->getTank().at(i).getconnectstatus() << setw(16) << engine->getTank().at(i).getvalvestatus() << endl;
	}
}

/**
 * This Function Prints Fuel Tanks Numbers
 */
void Simulation::print_fuel_tank_count()
{
	out << "There are " << engine->getTank().size() << " tanks in the system\n";
}

/**
 * This Function Prints Total Fuel Quentity
 */
void Simulation::print_total_fuel_quantity()
{
	double sum = engine->getEngineTank().getFuel_quantity();
	for (int i = 0; i < engine->getTank().size(); i++)
	{
		sum += engine->getTank().at(i).getFuel_quantity();
	}
	out << "Total fuel count is " << sum << endl;
}

/**
 * This Function Lists Connected Tanks
 */
void Simulation::list_connected_tanks()
{
	if (engine->getTank().size() == 0)
	{
		out << "No connected tanks to list.\n";
		return;
	}

	out << "Tank id     Capcaity     Current Quantity     IsBroken       Isconnect       Valve" << endl;
	for (int i = 0; i < engine->getTank().size(); i++)
	{
		if (engine->getTank().at(i).getconnectstatus())
		{
			out << engine->getTank().at(i).getId() << setw(16) << engine->getTank().at(i).getcapacity() << setw(16) << engine->getTank().at(i).getFuel_quantity();
			out << setw(16) << engine->getTank().at(i).isbroken() << setw(16) << engine->getTank().at(i).getconnectstatus() << setw(16) << engine->getTank().at(i).getvalvestatus() << endl;
		}
	}
}

/**
 * This Function Prints a Specific Tank's �nfo
 *@param tank_id gives the id of tank
 */
void Simulation::print_tank_info(int tank_id)
{
	if (engine->getTank().size() == 0)
	{
		out << "Tank " << tank_id << " was not found.\n";
		return;
	}
	int id = vector_id_finder(tank_id);
	if (id != -1)
	{
		out << "Tank id     Capcaity     Current Quantity     IsBroken       Isconnect       Valve" << endl;
		out << engine->getTank().at(id).getId() << setw(16) << engine->getTank().at(id).getcapacity() << setw(16) << engine->getTank().at(id).getFuel_quantity();
		out << setw(16) << engine->getTank().at(id).isbroken() << setw(16) << engine->getTank().at(id).getconnectstatus() << setw(16) << engine->getTank().at(id).getvalvestatus() << endl;
		return;
	}
}

/**
 * This Function Returns the Actual Queue
 *@param id_user gives the queue
 */
int Simulation::vector_id_finder(int id_user)
{
	int id = -1;
	for (int i = 0; i < engine->getTank().size(); i++) {
		if (engine->getTank().at(i).getId() == id_user)
		{
			id = i;
			break;
		}
	}
	return id;
}

/**
 * This Function Sets the state of operation
 */
void Simulation::set_state() {
	operation_state = false;
	notify();
}

/**
 * Attaches to observer list 
 */
void Simulation::attach(Update* observer) {
	observers.push_back(observer);
}

/**
 * Attaches to observer list (overload)
 */
void Simulation::attach(Update &observer) {
	observers.push_back(&observer);
}

/**
 * Detaches from observer list 
 */
void Simulation::detach(Update* observer) {
	for (int i = 0; i < observers.size(); i++) {
		if (observers[i]->getType() == observer->getType() && observers[i]->getId() == observer->getId()) {
			observers.erase(observers.begin() + i);
			break;
		}
	}
	/*observers.erase(observers.begin() + n);*/
}

/**
 * Sends notification to observers
 */
void Simulation::notify() {
	for (int i=0;i<observers.size();i++) {
		out<<observers[i]->update();
	}
}
