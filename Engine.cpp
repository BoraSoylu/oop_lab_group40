#include "Engine.h"
#include <iostream>

using namespace std;

/**
 * This Function Constructs the Engine Class
 */
Engine::Engine() {
	status = false;
	fuel_per_second = 5.5;
	total_consumed_fuel = 0;
	engineTank.setcapacity(55);
	setType("Engine");
	setId(0);
}

/**
 * This Function Deconstructs the Engine Class
 */
Engine::~Engine() {

}

/**
 * This Function Starts Engine
 */
void Engine::start_engine(void) {
	for (int i = 0; i < tank.size(); i++)
	{
		if(tank.at(i).getconnectstatus())
		{
			status = true;
			break;
		}
	}
}

/**
 * This Function Stops Engine
 */
void Engine::stop_engine(void) {
	give_back_fuel(engineTank.getFuel_quantity());
	status = false;
}

/**
 * This Function Gives Back Engine's Fuel To Tanks
 */
void Engine::give_back_fuel(double) {
	if (tank.size() == 0)
	{
		return;
	}
	
	double mincapacity;
	int i = 0;
	int holder = 0;

	for (  ; i < tank.size(); i++)
	{
		if (tank.at(i).getconnectstatus() && !tank.at(i).isbroken()&& tank.at(i).getvalvestatus())
		{
			mincapacity = tank.at(i).getFuel_quantity();
			holder = i;
			break;
		}
	}

	for (; i < tank.size(); i++)
	{
		if (tank.at(i).getconnectstatus() && !tank.at(i).isbroken() && tank.at(i).getvalvestatus())
		{
			if (tank.at(i).getFuel_quantity() < mincapacity)
			{
				mincapacity = tank.at(i).getFuel_quantity();
				holder = i;
			}
		}
	}

	tank.at(holder).fill_tank(engineTank.getFuel_quantity());
	engineTank.setFuel_quantity(0);
}

/**
 * This Function Removes the Fuel Tank
 */
void Engine::remove_fuel_tank(int tank_id)
{
	tank.erase(tank.begin()+tank_id);
	/*for (int i=0; i < tank.size(); i++) {
		if (tank_id == tank.at(i).getId()) {
			tank.erase(tank.begin()+i);
			break;
		}
	}*/
}

/**
 * This Function Adds a New Tank
 */
void Engine::add_fuel_tank(int capacity)
{
	Tank *temp=new Tank(capacity);
	tank.push_back(*temp);
}

/**
 * This Function Connects the Fuel Tank To The Engine
 */
void Engine::connect_fuel_tank_to_engine(int tank_id)
{
	for (int i = 0; i < tank.size(); i++) {
		if (tank_id == tank.at(i).getId()) {
			tank.at(i).Connect();
			return;
		}
	}
	cout << "Tank id does not found."<<endl;
}

/**
 * This Function Disconnects the Fuel Tank To The Engine
 */
void Engine::disconnect_fuel_tank_from_engine(int tank_id)
{
	for (int i = 0; i < tank.size(); i++) {
		if (tank_id == tank.at(i).getId()) {
			tank.at(i).Disconnect();
			return;
		}
	}
	cout << "Tank id does not found." << endl;
}

/**
 * This Function Consumes Fuel Along Given Seconds If The Engine is Running
 */
void Engine::wait(int seconds) {
	for (int i = 0; i < seconds; i++) {
		fuel_consumption();
	}
}

/**
 * This Function Burns Fuel
 */
void Engine::fuel_consumption(void) {

	if (engineTank.getFuel_quantity() < 20)
	{
		while(1)
		{
			int count = 0;
			for (int i = 0; i < tank.size(); i++)
			{
				if (tank.at(i).getconnectstatus() && !tank.at(i).isbroken() && tank.at(i).getFuel_quantity() != 0 && tank.at(i).getvalvestatus())
				{
					count++;
				}
			}
			
			if(count == 0)
			{
				break;
			}

			int pick = rand() % tank.size();
			if (tank.at(pick).getconnectstatus() && !tank.at(pick).isbroken() && tank.at(pick).getFuel_quantity() != 0 && tank.at(pick).getvalvestatus())
			{
				engineTank.setFuel_quantity(tank.at(pick).getFuel_quantity() + engineTank.getFuel_quantity());
				if (engineTank.getFuel_quantity() > engineTank.getcapacity())
				{
					tank.at(pick).setFuel_quantity(engineTank.getFuel_quantity() - engineTank.getcapacity());
					engineTank.setFuel_quantity(engineTank.getcapacity());
				}
				else
				{
					tank.at(pick).setFuel_quantity(0);
				}
			}

			if (engineTank.getFuel_quantity() >= 20)
			{
				break;
			}
		}
	}

	if (this->status && engineTank.getFuel_quantity() >= fuel_per_second) {
		engineTank.setFuel_quantity(engineTank.getFuel_quantity() - fuel_per_second);
		total_consumed_fuel += fuel_per_second;
	}
}

/**
 * This Function Gives the Total Consumed Fuel Quantity
 */
double Engine::gettotal_consumed_fuel_quantity(void) {
	return total_consumed_fuel;
}

/**
 * This Function Gives the Current Tank Vector
 */
vector<Tank> Engine::getTank() {
	return tank;
}

/**
 * This Function Sets the Tank Vector
 */
void Engine::setTank(vector<Tank> tank) {
	this->tank = tank;
}

/**
 * This Function Gives The Status Of Engine's Internal Tank
 */
bool Engine::getStatus() {
	return status;
}

/**
 * This Function Gives The Status Of Engine's Internal Tank Object
 */
Tank Engine::getEngineTank() {
	return engineTank;
}

/**
 * This Function prints that the simulation is stopped when the simulation is stopped
 */
string Engine::update() {
	return "Engine: Simulation stopped.\n";
}