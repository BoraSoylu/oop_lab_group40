#pragma once
#include "Engine.h"
#include "Tank.h"
#include<iostream>
#include<fstream>
#include<string.h>
#include<string>
#include<sstream>
#include<vector>
#include"Update.h"
using namespace std;
class Simulation
{
private:
	vector <Tank> temp;
	void readfile(string,string);
	string clearCommmand(string);
	int findNumber(string&);
	Engine *engine=Engine::getInstance();
	int find2Number(string &, int&);
	ifstream in;
	ofstream out;
	bool operation_state = true; 
	vector <Update*> observers;

public:
	Simulation(string, string);
	~Simulation();
	void stop_simulation();
	void print_total_consumed_fuel_quantity();
	void list_fuel_tanks();
	void print_fuel_tank_count();
	void print_total_fuel_quantity();
	void list_connected_tanks();
	void print_tank_info (int tank_id);
	int vector_id_finder(int id_user);
	void attach(Update*);
	void attach(Update&);
	void detach(Update*);
	void notify(); 
	void set_state(); 


};
